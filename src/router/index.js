import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

function lazyLoad (view) {
  return () => import('../views/' + view + '.vue')
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: lazyLoad('Home'),
    title: 'Audica'
  },
  {
    path: '/test',
    name: 'Test',
    component: lazyLoad('Test')
    // component: () => import('../views/Test.vue')
  },
  {
    path: '/simulation',
    name: 'symulacja',
    component: lazyLoad('Simulation')
    // component: () => import('../views/Test.vue')
  },
  {
    path: '/home',
    redirect: '/'
  },
  {
    path: '*',
    name: 'No Such Page',
    component: lazyLoad('err404')
  }
]

const router = new VueRouter({
  routes
  // mode: 'abstract'
})

export default router
