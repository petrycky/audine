# Audine - online hearing test
An engineering thesis project.

A goal of this project was to create an app that would serve as a medium fo monitoring current hearing state.

## View

You can view and use this app on [GitLab Pages](https://petrycky.gitlab.io/audine)

___
## Warnings:

Styled only for desktop.

Not applicable for medical purposes.

___

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
